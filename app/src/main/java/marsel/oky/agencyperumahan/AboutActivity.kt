package marsel.oky.agencyperumahan

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.about.*

class AboutActivity: AppCompatActivity(), View.OnClickListener{

    lateinit var v:View
    lateinit var preferences : SharedPreferences
    val RC_SUKSES : Int = 100
    val PREF_NAME = "Setting"
    val FIELD_FONT_SIZE_SUBJUDUL = "Font_Size_Subjudul"
    val FIELD_FONT_SIZE_DETAIL = "Font_Size_Detail"
    val FIELD_BG = "BACKGROUND"
    val FIELD_FONT_COLOR_HEADER = "COLOR_HEADER"
    val FIELD_TEXT = "text"
    var bg : String = ""
    var colorHead : String =""
    var fDetail : Int = 14
    var fSubjudul : Int = 24
    var judul : String = "Agency Perumahan"
    var detail : String = "Aplikasi ini digunakan oleh admin tiap Group Agency untuk mendata sebuah Perumahan dalam skala 1 Group Agency, yang terdapat cabang perumahan di beberapa tempat.\n\n\n"+"For More Maintenance Service,\n contact Developers :"
    val DEF_FONT_SIZE_SUBJUDUL = fSubjudul
    val DEF_FONT_SIZE_DETAIL = fDetail
    val DEF_JUDUL = judul
    val DEF_BG = bg
    val DEF_FONT_COLOR_HEADER = colorHead
    val DEF_DETAIL = detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        textView.setText(preferences.getString(FIELD_TEXT,DEF_JUDUL))
        textView7.setText(preferences.getString(FIELD_TEXT,DEF_DETAIL))
        fSubjudul = preferences.getInt(FIELD_FONT_SIZE_SUBJUDUL,DEF_FONT_SIZE_SUBJUDUL)
        fDetail = preferences.getInt(FIELD_FONT_SIZE_DETAIL,DEF_FONT_SIZE_DETAIL)
        bg = preferences.getString(FIELD_BG,DEF_BG).toString()
        colorHead = preferences.getString(FIELD_FONT_COLOR_HEADER,DEF_FONT_COLOR_HEADER).toString()
        val barcod = BarcodeEncoder()
        val bitmap = barcod.encodeBitmap("Thanks For Trust Us. @CopyrightMisbah-Oky",BarcodeFormat.QR_CODE,700,700)
        im_qr.setImageBitmap(bitmap)
        ig.setOnClickListener(this)
        fb.setOnClickListener(this)
        wa.setOnClickListener(this)
        mail.setOnClickListener(this)
        background()
        headerColor()
        UkDetail()
        UkSubjudul()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == RC_SUKSES)
                textView.setText(data?.extras?.getString("judul"))
            textView7.setText(data?.extras?.getString("detail"))
            bg = data?.extras?.getString("bgColor").toString()
            colorHead = data?.extras?.getString("headColor").toString()
            fSubjudul = data?.extras?.getInt("textSubjudul").toString().toInt()
            fDetail = data?.extras?.getInt("textDetail").toString().toInt()
            preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
            val prefEdit = preferences.edit()
            prefEdit.putInt(FIELD_FONT_SIZE_SUBJUDUL,fSubjudul)
            prefEdit.putInt(FIELD_FONT_SIZE_DETAIL,fDetail)
            prefEdit.putString(FIELD_TEXT,judul)
            prefEdit.putString(FIELD_TEXT,detail)
            prefEdit.putString(FIELD_BG,bg)
            prefEdit.putString(FIELD_FONT_COLOR_HEADER,colorHead)
            prefEdit.commit()

            background()
            headerColor()
            UkDetail()
            UkSubjudul()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var MenuInflater = menuInflater
        MenuInflater.inflate(R.menu.main2,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.act_tema->{
                var intent = Intent(this,SettingActivity::class.java)
                intent.putExtra("judul",textView.text.toString())
                intent.putExtra("detail",textView7.text.toString())
                startActivityForResult(intent,RC_SUKSES)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun background(){
        if(bg=="Blue"){
            about.setBackgroundColor(Color.BLUE)
        }
        else if(bg=="Yellow"){
            about.setBackgroundColor(Color.YELLOW)
        }
        else if(bg=="Green"){
            about.setBackgroundColor(Color.GREEN)
        }
        else if(bg=="Black"){
            about.setBackgroundColor(Color.BLACK)
        }
    }

    fun headerColor(){
        if(colorHead=="Blue"){
            textView.setTextColor(Color.BLUE)
            textView2.setTextColor(Color.BLUE)
            textView7.setTextColor(Color.BLUE)
        }
        else if(colorHead=="Yellow"){
            textView.setTextColor(Color.YELLOW)
            textView2.setTextColor(Color.YELLOW)
            textView7.setTextColor(Color.YELLOW)
        }
        else if(colorHead=="Green"){
            textView.setTextColor(Color.GREEN)
            textView2.setTextColor(Color.GREEN)
            textView7.setTextColor(Color.GREEN)
        }
        else if(colorHead=="Black"){
            textView.setTextColor(Color.BLACK)
            textView2.setTextColor(Color.BLACK)
            textView7.setTextColor(Color.BLACK)
        }
        else{
            textView.setTextColor(Color.RED)
        }
    }

    fun UkSubjudul(){
        textView2.textSize = fSubjudul.toFloat()
    }

    fun UkDetail(){
        textView7.textSize = fDetail.toFloat()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.ig ->{
                var link = "https://www.instagram.com/oke.junn/"
                var forward = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(forward)
            }
            R.id.fb ->{
                var link = "https://www.facebook.com/moh.surur.5"
                var forward = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(forward)
            }
            R.id.wa ->{
                var link = "https://api.whatsapp.com/send?phone=6285708264091"
                var forward = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(forward)
            }
            R.id.mail ->{
                var link = "mailto:misbahsurur99@gmail.com"
                var forward = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(forward)
            }
        }
    }
}