package marsel.oky.agencyperumahan

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_kontak.*

class AdapterCp (val dataCp: List<HashMap<String,String>>
                    , val cpFragment: KontakFragment
                ): RecyclerView.Adapter<AdapterCp.HolderDataCp>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterCp.HolderDataCp {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.row_cp,parent,false)
        return HolderDataCp(v)
    }

    override fun getItemCount(): Int {
        return dataCp.size
    }

    override fun onBindViewHolder(holder: AdapterCp.HolderDataCp, position: Int) {
        val data =dataCp.get(position)
        holder.row_id_cp.setText(data.get("id_cp"))
        holder.row_alamat_cp.setText(data.get("alamat"))
        holder.row_nama_cp.setText(data.get("nama"))
        holder.row_agency_cp.setText(data.get("agency"))
        holder.row_telp_cp.setText(data.get("telp"))
        holder.row_mail_cp.setText(data.get("email"))

        if (position.rem(2) == 0) holder.rowCp.setBackgroundColor(Color.rgb(230,245,240))
        else holder.rowCp.setBackgroundColor(Color.rgb(255,255,245))
        holder.rowCp.setOnClickListener(View.OnClickListener {
            cpFragment.tx_id_cp.setText(data.get("id_cp"))
            cpFragment.tx_sales_cp.setText(data.get("nama"))
            cpFragment.tx_alamat_cp.setText(data.get("alamat"))
            cpFragment.tx_agen_cp.setText(data.get("agency"))
            cpFragment.tx_telp_cp.setText(data.get("telp"))
            cpFragment.tx_email_cp.setText(data.get("email"))
        })

//        holder.rowCp.setOnClickListener(View.OnClickListener{
//            HomeFragment.tx_id.setText(data.get("id"))
//            HomeFragment.tx_nama.setText(data.get("nama"))
//            HomeFragment.tx_harga.setText(data.get("harga"))
//            Picasso.get().load(data.get("url")).into(HomeFragment.imageView2)
//        })
    }

    class HolderDataCp(v : View) :  RecyclerView.ViewHolder(v){
        val row_id_cp = v.findViewById<TextView>(R.id.row_id_cp)
        val row_alamat_cp = v.findViewById<TextView>(R.id.row_alamat_cp)
        val row_nama_cp = v.findViewById<TextView>(R.id.row_nama_cp)
        val row_agency_cp = v.findViewById<TextView>(R.id.row_agency_cp)
        val row_telp_cp = v.findViewById<TextView>(R.id.row_telp_cp)
        val row_mail_cp = v.findViewById<TextView>(R.id.row_mail_cp)
        val rowCp = v.findViewById<ConstraintLayout>(R.id.row_cp)
    }

}