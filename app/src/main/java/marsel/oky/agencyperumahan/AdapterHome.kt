package marsel.oky.agencyperumahan

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterHome (val dataHome: List<HashMap<String,String>>
                   //, val detilFragment: detilActivity
                    ): RecyclerView.Adapter<AdapterHome.HolderDataHome>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterHome.HolderDataHome {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.show_detil,parent,false)
        return HolderDataHome(v)
    }

    override fun getItemCount(): Int {
        return dataHome.size
    }

    override fun onBindViewHolder(holder: AdapterHome.HolderDataHome, position: Int) {
        val data =dataHome.get(position)
        holder.row_id.setText(data.get("id_perum"))
        holder.row_tipe.setText(data.get("nama_tipe"))
        holder.row_harga.setText(data.get("harga"))
        holder.row_agency.setText(data.get("agency"))
        holder.row_alamat.setText(data.get("alamat"))
        holder.row_des.setText(data.get("deskripsi"))
        holder.show_telp.setText(data.get("telp"))
        holder.show_cp.setText(data.get("nama"))
        holder.show_email.setText(data.get("email"))
       // holder.row_img.setImageURI(Uri.parse(data.get("url")))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.row_img);

        if (position.rem(2) == 0) holder.rowHome.setBackgroundColor(Color.rgb(230,245,240))
        else holder.rowHome.setBackgroundColor(Color.rgb(255,255,245))

//        holder.rowHome.setOnClickListener({
//            println("TEST")
//            val intent = Intent(holder.rowHome.context, detilActivity::class.java)
//            intent.putExtra("id_perum",id_perum)
//            startActivities(intent)
//        })

    }

    class HolderDataHome(v : View) :  RecyclerView.ViewHolder(v) {
        val row_img = v.findViewById<ImageView>(R.id.imv_detil)
        val row_id = v.findViewById<TextView>(R.id.show_id)
        val row_tipe = v.findViewById<TextView>(R.id.show_tipe)
        val row_harga = v.findViewById<TextView>(R.id.show_harga)
        val row_agency = v.findViewById<TextView>(R.id.show_agency)
        val row_alamat = v.findViewById<TextView>(R.id.show_alamat)
        val row_des = v.findViewById<TextView>(R.id.show_des)
        val show_cp = v.findViewById<TextView>(R.id.show_cp)
        val show_telp = v.findViewById<TextView>(R.id.show_telp)
        val show_email = v.findViewById<TextView>(R.id.show_mail)
        //val swoq_qr = v.findViewById<ImageView>(R.id.show_qr)
        val rowHome = v.findViewById<ConstraintLayout>(R.id.row_detil)


        }

    }



