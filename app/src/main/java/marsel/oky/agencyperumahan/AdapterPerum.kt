package marsel.oky.agencyperumahan

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_perumahan.*

class AdapterPerum(val dataPerum: List<HashMap<String,String>>
                    , val perumFragmen: PerumahanFragment
                    ): RecyclerView.Adapter<AdapterPerum.HolderDataPerum>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterPerum.HolderDataPerum {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.row_perum,parent,false)
        return HolderDataPerum(v)
    }

    override fun getItemCount(): Int {
        return dataPerum.size
    }

    override fun onBindViewHolder(holder: AdapterPerum.HolderDataPerum, position: Int) {
        val data =dataPerum.get(position)
        holder.row_id_perum.setText(data.get("id_perum"))
        holder.row_tipe_perum.setText(data.get("nama_tipe"))
        holder.row_harga_harga.setText(data.get("harga"))
        holder.row_agency_perum.setText(data.get("agency"))
        holder.row_des_perum.setText(data.get("deskripsi"))
        holder.row_telp_perum.setText(data.get("telp"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.row_imv_perum);

        if (position.rem(2) == 0) holder.rowPerum.setBackgroundColor(Color.rgb(230,245,240))
        else holder.rowPerum.setBackgroundColor(Color.rgb(255,255,245))

        holder.rowPerum.setOnClickListener(View.OnClickListener{
            val pos = perumFragmen.daftarCp.indexOf(data.get("agency"))
            val pis = perumFragmen.daftarTipe.indexOf(data.get("nama_tipe"))
            perumFragmen.tx_id_perum.setText(data.get("id_perum"))
            perumFragmen.sp_tipe.setSelection(pis)
            perumFragmen.sp_cp.setSelection(pos)
            perumFragmen.tx_des.setText(data.get("deskripsi"))
            Picasso.get().load(data.get("url")).into(perumFragmen.imUp_perum)
        })
    }

    class HolderDataPerum(v : View) :  RecyclerView.ViewHolder(v){
        val row_id_perum = v.findViewById<TextView>(R.id.row_id_perum)
        val row_tipe_perum = v.findViewById<TextView>(R.id.row_tipe_perum)
        val row_harga_harga = v.findViewById<TextView>(R.id.row_harga_perum)
        val row_agency_perum = v.findViewById<TextView>(R.id.row_agency_perum)
        val row_telp_perum = v.findViewById<TextView>(R.id.row_telp_perum)
        val row_des_perum = v.findViewById<TextView>(R.id.row_des_perum)
        val row_imv_perum = v.findViewById<ImageView>(R.id.row_imv_perum)
        val rowPerum = v.findViewById<ConstraintLayout>(R.id.row_perum)
    }

}