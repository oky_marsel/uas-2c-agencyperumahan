package marsel.oky.agencyperumahan

import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_home.view.ls_home
import org.json.JSONArray

class HomeFragment : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var v: View
    lateinit var adapterHome: AdapterHome
    lateinit var ft : FragmentTransaction
    lateinit var detilActivity: detilActivity
    var daftarHome = mutableListOf<HashMap<String,String>>()
    val url0 = "http://192.168.1.100/uas-2c-agencyperumahan-web/"
    val url2 = url0+"show_home.php"
    val url3 = url0+"show_detil.php"
    var id_perum=""
    var harga=""
    var nama_tipe=""
    var alamat=""
    var deskripsi=""
    var url=""
    var agency=""
    var nama=""
    var email=""
    var telp=""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        detilActivity = detilActivity()
//        thisParent.row_id=""
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_home,container,false)
        adapterHome = AdapterHome(daftarHome)
        v.ls_home.layoutManager = LinearLayoutManager(thisParent)
        v.ls_home.adapter = adapterHome


        return v
    }

    override fun onStart() {
        super.onStart()
        show()
    }

    fun show(){
        val request = StringRequest(
            Request.Method.POST,url2, Response.Listener { response ->
                daftarHome.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mn = HashMap<String,String>()
                    mn.put("id_perum",jsonObject.getString("id_perum"))
                    mn.put("nama_tipe",jsonObject.getString("nama_tipe"))
                    mn.put("harga",jsonObject.getString("harga"))
                    mn.put("agency",jsonObject.getString("agency"))
                    mn.put("alamat",jsonObject.getString("alamat"))
                    mn.put("deskripsi",jsonObject.getString("deskripsi"))
                    mn.put("nama",jsonObject.getString("nama"))
                    mn.put("telp",jsonObject.getString("telp"))
                    mn.put("email",jsonObject.getString("email"))
                    mn.put("url",jsonObject.getString("url"))
                    daftarHome.add(mn)
                }
                adapterHome.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun getdetil(){
        val request = StringRequest(
            Request.Method.POST,url3, Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    id_perum = jsonObject.getString("id_perum")
                    harga= jsonObject.getString("harga")
                    nama_tipe=jsonObject.getString("nama_tipe")
                    alamat=jsonObject.getString("alamat")
                    deskripsi=jsonObject.getString("deskripsi")
                    url=jsonObject.getString("url")
                    agency=jsonObject.getString("agency")
                    nama=jsonObject.getString("nama")
                    email=jsonObject.getString("email")
                    telp=jsonObject.getString("telp")
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

//    fun detil(){
//        thisParent.row_id= show_id.toString()
//
//        ft = childFragmentManager.beginTransaction()
//        ft.replace(R.id.FrameLayout,detilActivity).commit()
//        v.FrameLayout.visibility=View.VISIBLE
//    }

}