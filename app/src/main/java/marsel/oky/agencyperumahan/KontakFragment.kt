package marsel.oky.agencyperumahan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_kontak.*
import kotlinx.android.synthetic.main.fragment_kontak.view.*
import kotlinx.android.synthetic.main.fragment_kontak.view.tx_id_cp
import kotlinx.android.synthetic.main.fragment_perumahan.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

@Suppress("UNREACHABLE_CODE")
class KontakFragment : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var v: View
    lateinit var adapterCp: AdapterCp
    var daftarCp = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.1.100/uas-2c-agencyperumahan-web/"
    val url2 = url+"show_cp.php"
    val url3 = url+"query_cp.php"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_kontak,container,false)
        adapterCp = AdapterCp(daftarCp,this)
        v.ls_cp.layoutManager = LinearLayoutManager(thisParent)
        v.ls_cp.adapter = adapterCp
        v.bt_cp.setOnClickListener (this)
        v.up_cp.setOnClickListener(this)
        v.del_cp.setOnClickListener (this)


        return v
    }

    override fun onStart() {
        super.onStart()
        show()
    }

    fun show(){
        val request = StringRequest(
            Request.Method.POST,url2, Response.Listener { response ->
                daftarCp.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    val mn = HashMap<String,String>()
                    mn.put("id_cp",jsonObject.getString("id_cp"))
                    mn.put("alamat",jsonObject.getString("alamat"))
                    mn.put("nama",jsonObject.getString("nama"))
                    mn.put("agency",jsonObject.getString("agency"))
                    mn.put("telp",jsonObject.getString("telp"))
                    mn.put("email",jsonObject.getString("email"))
                    daftarCp.add(mn)
                }
                adapterCp.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queu = Volley.newRequestQueue(thisParent)
        queu.add(request)
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    show()
                    daftarCp.clear()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                    show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("id_cp",tx_id_cp.text.toString())
                        hm.put("nama",tx_sales_cp.text.toString())
                        hm.put("alamat",tx_alamat_cp.text.toString())
                        hm.put("agency",tx_agen_cp.text.toString())
                        hm.put("telp",tx_telp_cp.text.toString())
                        hm.put("email",tx_email_cp.text.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_cp",tx_id_cp.text.toString())
                        hm.put("nama",tx_sales_cp.text.toString())
                        hm.put("alamat",tx_alamat_cp.text.toString())
                        hm.put("agency",tx_agen_cp.text.toString())
                        hm.put("telp",tx_telp_cp.text.toString())
                        hm.put("email",tx_email_cp.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_cp",tx_id_cp.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_cp -> {
                query("insert")
                daftarCp.clear()
                show()
            }
            R.id.up_cp -> {
                query("update")
                daftarCp.clear()
                show()
            }
            R.id.del_cp -> {
                query("delete")
                daftarCp.clear()
                show()
            }
        }
    }

}