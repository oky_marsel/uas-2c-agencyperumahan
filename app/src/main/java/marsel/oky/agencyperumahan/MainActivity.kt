package marsel.oky.agencyperumahan

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.NavHostFragment.findNavController
import kotlinx.android.synthetic.main.about.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    var row_id=""
    val RC_SUKSES : Int = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home, R.id.nav_perum, R.id.nav_kontak, R.id.nav_video, R.id.nav_about), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        var mnInflat = menuInflater
        mnInflat.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.act_about->{
                val intent = Intent(this,AboutActivity::class.java)
                startActivity(intent)
            }
//            R.id.act_tema->{
//                val intent = Intent(this,SettingActivity::class.java)
////                intent.putExtra("judul",textView.text.toString())
////                intent.putExtra("detail",textView7.text.toString())
//                startActivityForResult(intent,RC_SUKSES)
//            }
        }
    //    drawer_layout.closeDrawer(GravityCompat.START)
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

//    override fun onNavigationItemSelected(item: MenuItem): Boolean {
//        when(item?.itemId){
//            R.id.nav_about ->{
//                val intent = Intent(this,AboutActivity::class.java)
//                startActivity(intent)
//            }
//            R.id.nav_video->{
//                val intent = Intent(this,VideoActivity::class.java)
////                intent.putExtra("judul",textView.text.toString())
////                intent.putExtra("detail",textView7.text.toString())
//                startActivityForResult(intent,RC_SUKSES)
//
//            }
//        }
//        drawer_layout.closeDrawer(GravityCompat.START)
//        return true
//    }
}
