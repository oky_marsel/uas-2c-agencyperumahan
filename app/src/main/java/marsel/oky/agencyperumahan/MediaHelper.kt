package marsel.oky.agencyperumahan

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Base64
import android.widget.ImageView
import androidx.annotation.RequiresApi
import java.io.ByteArrayOutputStream

class MediaHelper(context : Context) {
    val context = context
    fun getRcGallery() : Int{
        return REQ_CODE_GALLERY
    }
    fun bitmapToString(bmp : Bitmap) : String{
        val outputStream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG,60,outputStream)
        val byteArray = outputStream.toByteArray()
        return  android.util.Base64.encodeToString(byteArray, Base64.DEFAULT)
    }
   // @RequiresApi(Build.VERSION_CODES.P)
    fun  getBitmapToString(uri: Uri?, imv : ImageView) : String{
//        var bmp = when {
//            Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
//                this.context.contentResolver,
//                uri
//            )
//            else -> {
//                val source = ImageDecoder.createSource(this.context.contentResolver, uri)
//                ImageDecoder.decodeBitmap(source)
//            }
//        }
        var bmp = MediaStore.Images.Media.getBitmap(
            this.context.contentResolver,uri)
        var dim = 720
        if (bmp.height > bmp.width){
            bmp = Bitmap.createScaledBitmap(bmp,
                (bmp.width*dim).div(bmp.height),dim,true)
        }else{
            bmp = Bitmap.createScaledBitmap(bmp,
                (bmp.width*dim).div(bmp.height),dim,true)
        }
        imv.setImageBitmap(bmp)
        return bitmapToString(bmp)
    }
    companion object{
        const val REQ_CODE_GALLERY = 100
    }
}