package marsel.oky.agencyperumahan

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_perumahan.*
import kotlinx.android.synthetic.main.fragment_perumahan.view.*
import kotlinx.android.synthetic.main.fragment_perumahan.view.sp_cp
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class PerumahanFragment : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var v: View
    lateinit var dialog: AlertDialog.Builder
    lateinit var adapterPerum: AdapterPerum
    lateinit var mediaHelper: MediaHelper
    lateinit var cpAdapter : ArrayAdapter<String>
    lateinit var tipeAdapter : ArrayAdapter<String>
    var daftarPerum = mutableListOf<HashMap<String,String>>()
    var daftarCp = mutableListOf<String>()
    var daftarTipe = mutableListOf<String>()
    val url = "http://192.168.1.100/uas-2c-agencyperumahan-web/"
    val url2 = url+"show_perum.php"
    val url3 = url+"query_perum.php"
    val url4 = url+"show_tipe.php"
    val url5 = url+"show_cp.php"
    var imstr = ""
    var pilihCp = ""
    var pilihtipe = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_perumahan,container,false)
        dialog = AlertDialog.Builder(thisParent)
        adapterPerum = AdapterPerum(daftarPerum,this)
        mediaHelper =  MediaHelper(thisParent)
        v.ls_perum.layoutManager = LinearLayoutManager(thisParent)
        v.ls_perum.adapter = adapterPerum
        cpAdapter = ArrayAdapter(thisParent,android.R.layout.simple_dropdown_item_1line, daftarCp)
        v.sp_cp.adapter = cpAdapter
        v.sp_cp.onItemSelectedListener = itemselectedCP
        tipeAdapter = ArrayAdapter(thisParent,android.R.layout.simple_dropdown_item_1line, daftarTipe)
        v.sp_tipe.adapter = tipeAdapter
        v.sp_tipe.onItemSelectedListener = itemselectedTipe
        v.imUp_perum.setOnClickListener(this)
        v.up_perum.setOnClickListener(this)
        v.add_perum.setOnClickListener(this)
        v.del_perum.setOnClickListener(this)


        return v
    }

    override fun onStart() {
        super.onStart()
        show()
        getnamaCp()
        getnamaTipe()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUp_perum -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.add_perum -> {
                queryCRUD("insert")
                show()

            }
            R.id.del_perum -> {
                queryCRUD("delete")
                show()
            }
            R.id.up_perum ->{
                queryCRUD("update")
                show()
            }
        }
    }

    fun show(){
        val request = StringRequest(
            Request.Method.POST,url2, Response.Listener { response ->
                daftarPerum.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mn = HashMap<String,String>()
                    mn.put("id_perum",jsonObject.getString("id_perum"))
                    mn.put("nama_tipe",jsonObject.getString("nama_tipe"))
                    mn.put("harga",jsonObject.getString("harga"))
                    mn.put("agency",jsonObject.getString("agency"))
                    mn.put("telp",jsonObject.getString("telp"))
                    mn.put("deskripsi",jsonObject.getString("deskripsi"))
                    mn.put("url",jsonObject.getString("url"))
                    daftarPerum.add(mn)
                }
                adapterPerum.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queu = Volley.newRequestQueue(thisParent)
        queu.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imstr = mediaHelper.getBitmapToString(data!!.data,imUp_perum)
            }
        }
    }


    val itemselectedCP = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp_cp.setSelection(0)
            pilihCp = daftarCp.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihCp = daftarCp.get(position)
        }
    }

    val itemselectedTipe = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp_tipe.setSelection(0)
            pilihtipe = daftarTipe.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihtipe = daftarTipe.get(position)
        }
    }

    fun getnamaCp(){
        val request = StringRequest(Request.Method.POST,url5,
            Response.Listener { response ->
                daftarCp.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarCp.add(jsonObject.getString("agency"))
                }
                cpAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun getnamaTipe(){
        val request = StringRequest(Request.Method.POST,url4,
            Response.Listener { response ->
                daftarTipe.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarTipe.add(jsonObject.getString("nama_tipe"))
                }
                tipeAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            })
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun queryCRUD(mode : String){
        val request = object  : StringRequest(Method.POST,url3,
            Response.Listener {  response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    show()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal",Toast.LENGTH_LONG).show()
                    show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent, "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT)
                    .show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_perum",tx_id_perum.text.toString())
                        hm.put("nama_tipe",pilihtipe)
                        hm.put("images",imstr)
                        hm.put("file",nmFile)
                        hm.put("agency",pilihCp)
                        hm.put("deskripsi",tx_des.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_perum",tx_id_perum.text.toString())
                        hm.put("nama_tipe",pilihtipe)
                        hm.put("images",imstr)
                        hm.put("file",nmFile)
                        hm.put("agency",pilihCp)
                        hm.put("deskripsi",tx_des.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_perum",tx_id_perum.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

}