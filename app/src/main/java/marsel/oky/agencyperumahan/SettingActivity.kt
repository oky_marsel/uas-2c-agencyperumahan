package marsel.oky.agencyperumahan

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    val arrWarna = arrayOf("Blue","Yellow","Green","Black")
    lateinit var adapterSpin : ArrayAdapter<String>
    var Fheader : String = ""
    var fSubJud : Int = 0
    var fDetail : Int = 0
    var x : String = ""
    var y : String = ""
    var bg : String = ""


    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fSubJud = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }
    val onSeek2 = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fDetail = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrWarna)
        sp_Bg.adapter = adapterSpin
        rgWarna.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_Blue -> Fheader = "Blue"
                R.id.rb_Yellow -> Fheader = "Yellow"
                R.id.rb_Green -> Fheader = "Green"
                R.id.rb_Black -> Fheader = "Black"
            }
            btnSimpan.setOnClickListener(this)
        }
        sb_Fsubtitle.setOnSeekBarChangeListener(onSeek)
        sb_Fdetail.setOnSeekBarChangeListener(onSeek2)
        var paket: Bundle? = intent.extras
        ed_judul.setText(paket?.getString("judul"))
        ed_detail.setText(paket?.getString("detail"))
        sp_Bg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(baseContext, adapterSpin.getItem(position), Toast.LENGTH_SHORT)
                    .show()
                bg = adapterSpin.getItem(sp_Bg.selectedItemPosition).toString()
            }
        }
    }
    override fun onClick(v: View?) {
        x = ed_judul.text.toString()
        y = ed_detail.text.toString()
        Toast.makeText(this, "Perubahan Telah Disimpan", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun finish() {
        var intent = Intent()
        intent.putExtra("bgColor",bg)
        intent.putExtra("headColor",Fheader)
        intent.putExtra("textSubjudul",fSubJud)
        intent.putExtra("textDetail",fDetail)
        intent.putExtra("judul",x)
        intent.putExtra("detail",y)
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }
}