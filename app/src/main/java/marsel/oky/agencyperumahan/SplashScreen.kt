package marsel.oky.agencyperumahan

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SplashScreen : AppCompatActivity(){

    lateinit var topAnim: Animation
    lateinit var bottomAnim: Animation
    lateinit var image: ImageView
    lateinit var  logo: TextView
    lateinit var  slogan: TextView
    private lateinit var H: Handler
    private lateinit var r: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation)
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation)

        image = findViewById(R.id.logo)
        logo = findViewById(R.id.title)
        slogan = findViewById(R.id.subtitle)

        image.setAnimation(topAnim)
        logo.setAnimation(bottomAnim)
        slogan.setAnimation(bottomAnim)

        H = Handler()
        r = Runnable {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        H.postDelayed(r,5000)
    }
}